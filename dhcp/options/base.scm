;;; GNU Guix DHCP Client.
;;;
;;; Copyright � 2015 Rohan Prinja <rohan.prinja@gmail.com>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dhcp options base)
  #:use-module (srfi srfi-9)
  #:use-module (rnrs bytevectors)
  #:export (<dhcp-option>
	    make-dhcp-option
	    dhcp-option?
	    dhcp-option-code set-dhcp-option-code!
	    dhcp-option-len set-dhcp-option-len!
	    dhcp-option-val set-dhcp-option-val!))

; DHCP option object.
; See RFC 2132 for a list of DHCP options.
(define-record-type <dhcp-option>
  (make-dhcp-option code len val)
  dhcp-option?
  (code dhcp-option-code set-dhcp-option-code!)
  (len dhcp-option-len set-dhcp-option-len!)
  (val dhcp-option-val set-dhcp-option-val!))

; code = option code, lies between 0 and 255
; len = length of option in bytes when it is serialized into a,
; bytevector, excluding 'code' and 'len' fields
; val = bytevector of length 'len' containing the option value
; name = option name, stored as a symbol

; Note: the actual net length of the option when serialized as a
; bytevector is (+ len 2), and not 'len', because 'code' and
; 'len' take up one byte each. Exceptions to this are the
; 'Pad' and 'End' options, both of which are 1 byte only.
; For such one-byte options, 'len' is 0 and 'val' is #f.
