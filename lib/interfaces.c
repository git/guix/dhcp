/// GNU Guix DHCP Client.
///
/// Copyright � 2015 Rohan Prinja <rohan.prinja@gmail.com>
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 3 of the License, or (at
/// your option) any later version.
///
/// This program is distributed in the hope that it will be useful, but
/// WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <sys/types.h>
#include <ifaddrs.h>
#include <stdio.h>

char* get_sockaddr_data(struct ifaddrs* ifaddr) {
    return ifaddr->ifa_addr->sa_data;
}

struct ifaddrs* get_first_interface_ptr() {
    struct ifaddrs *ifaddr, *itr;
    if (getifaddrs(&ifaddr) == -1) {
        return NULL;
    }
    return ifaddr;
}

void free_interfaces(struct ifaddrs *ifaddr) {
    freeifaddrs(ifaddr);
}
