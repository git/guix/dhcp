;;; GNU Guix DHCP Client.
;;;
;;; Copyright � 2015 Rohan Prinja <rohan.prinja@gmail.com>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-load-path (string-append (dirname (current-filename))
				 "/.."))

(define-module (test-dhcp-send))

(use-modules (srfi srfi-64)
	     (dhcp send))

(test-begin "dhcp-send")

'TODO

(define sock (make-dgram-sock))

(if (= (fileno sock) -1)
    (begin
      (display "error creating sock\n")
      (exit)))

; Enable broadcast for the socket.
(setsockopt sock SOL_SOCKET SO_BROADCAST 1)

; Default broadcast address for DHCP.
(define broadcast-sockaddr
  (make-broadcast-sockaddr *client-out-port*))

(let ((bytes-sent (sendto sock
			  #vu8(1 2 3 4)
			  broadcast-sockaddr)))
  (test-eqv "sendto-works"
	    bytes-sent
	    4))

(test-end)

(exit (zero? (test-runner-fail-count (test-runner-current))))
