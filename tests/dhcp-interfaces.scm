;;; GNU Guix DHCP Client.
;;;
;;; Copyright � 2015 Rohan Prinja <rohan.prinja@gmail.com>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-dhcp-interfaces))

(use-modules (srfi srfi-64)
	     (dhcp interfaces)
	     (arp identifiers)
	     ((guix build syscalls) #:select (all-network-interfaces))
	     ((ice-9 popen) #:select (open-pipe*))
	     ((ice-9 rdelim) #:select (read-line))
	     ((srfi srfi-1) #:select (last)))

(test-begin "dhcp-interfaces")

(define ifaces (all-network-interfaces))

(define eth (car (retain-ethernet-interfaces ifaces)))

(define netif
  (make-network-interface
   (car
    (retain-ethernet-interfaces
     (all-network-interfaces)))
   'DHCP-INIT))

(define (ifconfig-find-hwaddr name)
  "Find the hwaddr string of an interface for
which NAME is a prefix of the interface name"
  (let ((pipe (open-pipe* OPEN_READ "ifconfig")))
    (let lp ((line (read-line pipe)))
      (if (string-prefix? "eth" line)
	  (let* ((trimmed-line (string-trim-both line))
		 (split-line (string-split trimmed-line #\space))
		 (hwaddr (last split-line)))
	    hwaddr)
	  (lp (read-line pipe))))))

(print-hardware-address (hardware-address eth))

(test-eq "correct-family"
	 ARPHRD_ETHER
	 (hardware-family eth))

(test-assert "netif-correct-family"
	     (string-prefix? "eth"
			     (net-iface-name netif)))

(test-equal "correct-hwaddr"
	    (hardware-address-to-string (hardware-address eth))
	    (ifconfig-find-hwaddr "eth"))

(test-equal "correct-hwaddr-2"
	    (hardware-address-to-string (car (net-iface-hwaddr netif)))
	    (ifconfig-find-hwaddr "eth"))

(test-end)

(exit (zero? (test-runner-fail-count (test-runner-current))))
