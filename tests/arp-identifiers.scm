;;; GNU Guix DHCP Client.
;;;
;;; Copyright 2015 Free Software Foundation, Inc.
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-load-path (string-append (dirname (current-filename))
				 "/.."))

(define-module (test-arp-identifiers))

(use-modules (srfi srfi-64)
	     (arp identifiers))

(test-begin "arp-identifiers")

(test-eq "idempotence"
	 'ARPHRD_IEEE1394
	 (map-code-to-ident ARPHRD_IEEE1394))

(test-equal "correct-mapping" ARPHRD_ETHER 1)

(test-end)

(exit (zero? (test-runner-fail-count (test-runner-current))))
